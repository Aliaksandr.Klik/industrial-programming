package org.project;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

public class Student {
    String name;
    int year;
    int group;
    ArrayList<Session> sessions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public ArrayList<Session> getSessions() {
        return sessions;
    }

    public void setSessions(ArrayList<Session> sessions) {
        this.sessions = sessions;
    }

    public void addSession(Session session) {
        sessions.add(session);
    }

    public Student() {
        name = "";
        year = -1;
        group = -1;
        sessions = null;
    }

    public Student(String name, int year, int group, ArrayList<Session> sessions) {
        this.name = name;
        this.year = year;
        this.group = group;
        this.sessions = sessions;
    }

    @Override
    public String toString() {
        return "Student{" + "name=" + name + ", year=" + year + ", group=" + group + ", sessions=" + sessions + '}';
    }

    public String toJson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(this);
    }

    public static Student fromJson(String studentJson) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(studentJson, Student.class);
    }
}
