import java.text.*;
import java.util.Scanner;

public class Main {
    public static double taylor_cos(double x, int prec) {
        x %= 2 * Math.PI;
        double sum = 1, step = 1, epsilon = Math.pow(10, -prec), i = 1;
        while (Math.abs(step) >= epsilon) {
            step *= -x * x / ((2 * i - 1) * (2 * i));
            sum += step;
            ++i;
        }
        return sum;
    }

    public static void main(String[] args) {
        double x;
        int prec;
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter x: ");
        x = scanner.nextDouble();

        System.out.print("Enter precision. It must be lower or equal to 15: ");
        prec = scanner.nextInt();

        while (prec < 0 || prec > 15) {
            System.out.print("Incorrect precision. It must be lower or equal to 15: ");
            prec = scanner.nextInt();
        }

        NumberFormat formatter = NumberFormat.getNumberInstance();
        formatter.setMaximumFractionDigits(prec);
        System.out.println("My implementation:       " + formatter.format(taylor_cos(x, prec)));
        System.out.println("Default implementation:  " + formatter.format(Math.cos(x)));
    }
}

