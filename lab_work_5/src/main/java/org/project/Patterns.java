package org.project;

import java.util.regex.Pattern;

public class Patterns {
    static public final Pattern entryPattern = Pattern.compile("(\\d+) entr(:?y|ies)");
    static public final Pattern studentPattern = Pattern.compile("(\\w+), (\\d+) year, (\\d+) group, (\\d+) sessions?");
    static public final Pattern sessionPattern = Pattern.compile("Session (\\d+): (\\d+) exams? \\+ (\\d+) credits?");
    static public final Pattern examPattern = Pattern.compile("(.+?) - (.+?) - (\\d+)");
    static public final Pattern creditPattern = Pattern.compile("(.+?) - (.+?) - (Passed|Failed)");

}
