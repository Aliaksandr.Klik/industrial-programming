package org.project;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Exam {
    String teacher;
    String subject;
    int grade;

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public Exam() {
        teacher = "";
        subject = "";
        grade = -1;
    }

    public Exam(String teacher, String subject, int grade) {
        this.teacher = teacher;
        this.subject = subject;
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "Exam{" + "teacher=" + teacher + ", subject=" + subject + ", grade=" + grade + '}';
    }

    public String toJson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(this);
    }

    public static Exam fromJson(String json) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, Exam.class);
    }
}
