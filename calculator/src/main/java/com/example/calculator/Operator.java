package com.example.calculator;

public enum Operator {
    NOP,
    ADD,
    SUB,
    MUL,
    DIV;

    @Override
    public String toString() {
        return switch (this) {
            case ADD -> "+";
            case SUB -> "-";
            case MUL -> "*";
            case DIV -> "/";
            default -> "";
        };
    }

    public static Operator fromString(String operatorString) {
        return switch (operatorString) {
            case "+" -> ADD;
            case "-" -> SUB;
            case "*" -> MUL;
            case "/" -> DIV;
            default -> NOP;
        };
    }
}
