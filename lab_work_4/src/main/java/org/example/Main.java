package org.example;

import java.io.*;
import java.util.*;
import java.lang.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input file name: ");
        String inputFileName = scanner.nextLine();
        System.out.print("Output file name: ");
        String outputFileName = scanner.nextLine();
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(inputFileName);
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
            System.exit(1);
        }
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(outputFileName);
        } catch (IOException e) {
            System.out.println("File not found!");
            System.exit(1);
        }
        Scanner fileScanner = new Scanner(fileReader);
        Exam exam = new Exam();
        ArrayList<Exam> exams = new ArrayList<>();
        while (fileScanner.hasNextLine()) {
            String line = fileScanner.nextLine();
            if (line.isEmpty()) {
                exams.add(exam);
                exam = new Exam();
            } else {
                if (!exam.getName().isEmpty()) {
                    exams.add(exam);
                }
                exam = Exam.parse(line);
            }
        }
        if (!exam.getName().isEmpty()) {
            exams.add(exam);
        }
        System.out.println();
        System.out.println("Before sorting by grade:");
        writeExams(fileWriter, exams);
        //        Collections.sort(exams, Exam.GradeComparator);
        exams.sort(Comparator.comparing(Exam::getGrade));
        System.out.println("After sorting by grade:");
        for (Exam exam1 : exams) {
            try {
                fileWriter.write(exam1.toString() + "\n");
                System.out.println(exam1);
            } catch (IOException e) {
                System.out.println("File not found!");
                System.exit(1);
            }
        }
        exams.sort(Comparator.comparing(Exam::getName));
        System.out.println();
        System.out.println("After sorting by exam names:");
        writeExams(fileWriter, exams);
        System.out.println("What exam would you like to find?");
        String examName = scanner.nextLine();
        System.out.println();
        System.out.println("Exams with name " + examName + ":");
        int index = Collections.binarySearch(exams, new Exam("", examName, 0), Comparator.comparing(Exam::getName));
        if (index < 0) {
            System.out.println("Exam not found!");
        } else {
            while (index > 0 && exams.get(index - 1).getName().equals(examName)) {
                index--;
            }
            while (index < exams.size() && exams.get(index).getName().equals(examName)) {
                System.out.println(exams.get(index));
                index++;
            }
        }

        try {
            fileReader.close();
        } catch (IOException e) {
            System.out.println("File not found!");
            System.exit(1);
        }
        try {
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("File not found!");
            System.exit(1);
        }
    }

    private static void writeExams(FileWriter fileWriter, ArrayList<Exam> exams) {
        for (Exam exam1 : exams) {
            try {
                fileWriter.write(exam1.toString() + "\n");
                System.out.println(exam1);
            } catch (IOException e) {
                System.out.println("File not found!");
                System.exit(1);
            }
        }
        System.out.println();
    }
}