package org.project;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;

public class University {
    private ArrayList<Student> students;

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    public University() {
        students = null;
    }

    public University(ArrayList<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("University{\n\tstudents=[\n");
        for (Student student : students) {
            sb.append(student).append("\n");
        }
        sb.append("\t]\n}");
        return sb.toString();
    }

    public String toJson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(this);
    }

    public static University fromJson(String universityJson) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(universityJson, University.class);
    }

    public static University fromFile(String filename) throws IOException, IllegalArgumentException {
        FileReader fr = new FileReader(filename);
        BufferedReader br = new BufferedReader(fr);

        ArrayList<Student> students = new ArrayList<>();

        String line = br.readLine();
        Matcher matcher = Patterns.entryPattern.matcher(line);
        if (!matcher.matches()) throw new IllegalArgumentException("File format is incorrect, line \"" + line + "\" was not matched");

        int entries = Integer.parseInt(matcher.group(1));
        while (entries != 0) {
            Student student = new Student();
            ArrayList<Session> sessions = new ArrayList<>();

            line = br.readLine();
            matcher = Patterns.studentPattern.matcher(line);
            if (!matcher.matches()) throw new IllegalArgumentException("File format is incorrect, line \"" + line + "\" was not matched");
            student.setName(matcher.group(1));
            student.setYear(Integer.parseInt(matcher.group(2)));
            student.setGroup(Integer.parseInt(matcher.group(3)));
            int sessionsNumber = Integer.parseInt(matcher.group(4));

            while (sessionsNumber != 0) {
                Session session = new Session();
                ArrayList<Exam> exams = new ArrayList<>();
                ArrayList<Credit> credits = new ArrayList<>();

                line = br.readLine();
                matcher = Patterns.sessionPattern.matcher(line);
                if (!matcher.matches()) throw new IllegalArgumentException("File format is incorrect, line \"" + line + "\" was not matched");
                session.setSessionNumber(Integer.parseInt(matcher.group(1)));
                int examsNumber = Integer.parseInt(matcher.group(2));
                int creditsNumber = Integer.parseInt(matcher.group(3));

                while (examsNumber != 0) {
                    Exam exam = new Exam();

                    line = br.readLine();
                    matcher = Patterns.examPattern.matcher(line);
                    if (!matcher.matches()) throw new IllegalArgumentException("File format is incorrect, line \"" + line + "\" was not matched");
                    exam.setSubject(matcher.group(1));
                    exam.setTeacher(matcher.group(2));
                    exam.setGrade(Integer.parseInt(matcher.group(3)));
                    exams.add(exam);
                    examsNumber--;
                }

                while (creditsNumber != 0) {
                    Credit credit = new Credit();

                    line = br.readLine();
                    matcher = Patterns.creditPattern.matcher(line);
                    if (!matcher.matches()) throw new IllegalArgumentException("File format is incorrect, line \"" + line + "\" was not matched");
                    credit.setSubject(matcher.group(1));
                    credit.setTeacher(matcher.group(2));
                    credit.setPassed(matcher.group(3).equals("Passed"));
                    credits.add(credit);
                    creditsNumber--;
                }

                session.setExams(exams);
                session.setCredits(credits);
                sessions.add(session);
                sessionsNumber--;
            }

            student.setSessions(sessions);
            students.add(student);
            entries--;
        }

        return new University(students);
    }
}
