package org.project;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Credit {
    String teacher;
    String subject;
    boolean passed;

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public boolean isPassed() {
        return passed;
    }

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    public Credit() {
        teacher = "";
        subject = "";
        passed = false;
    }

    public Credit(String teacher, String subject, boolean passed) {
        this.teacher = teacher;
        this.subject = subject;
        this.passed = passed;
    }

    @Override
    public String toString() {
        return "Credit{" + "teacher=" + teacher + ", subject=" + subject + ", passed=" + passed + '}';
    }

    public String toJson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(this);
    }

    public static Credit fromJson(String json) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, Credit.class);
    }
}
