package org.project;

import java.io.FileWriter;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        University university;
        try {
            university = University.fromFile("input.txt");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return;
        }

        System.out.println(university);

        FileWriter textWriter;
        try {
            textWriter = new FileWriter("output.txt");
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }

        try {
            textWriter.write(university.toString());
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }

        try {
            textWriter.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }

        System.out.println("University saved to output.txt");

        FileWriter jsonWriter;
        try {
            jsonWriter = new FileWriter("output.json");
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }

        try {
            jsonWriter.write(university.toJson());
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }

        try {
            jsonWriter.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }
        System.out.println("University saved to output.json\n");

        System.out.println("High graders:");
        FileWriter goodStudentsWriter;
        try {
            goodStudentsWriter = new FileWriter("goodStudents.txt");
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }

        for (Student student : university.getStudents()) {
            boolean goodStudent = true;
            for (Session session : student.getSessions()) {
                for (Exam exam : session.getExams()) {
                    if (exam.getGrade() < 7) {
                        goodStudent = false;
                        break;
                    }
                }
                if (!goodStudent) break;
                for (Credit credit : session.getCredits()) {
                    if (!credit.isPassed()) {
                        goodStudent = false;
                        break;
                    }
                }
                if (!goodStudent) break;
            }
            if (goodStudent) {
                try {
                    System.out.println(student);
                    goodStudentsWriter.write(student.toString());
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                    return;
                }
            }
        }

        try {
            goodStudentsWriter.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}