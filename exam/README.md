# Exam work

This is a simple Java application for managing a list of hotels. The application reads hotel data from a text file, sorts the hotels by city and stars, and writes the sorted list to a JSON file. It also provides a simple console interface for searching hotels.

## Features

1. Reading hotel data from a text file.
2. Sorting hotels by city and stars.
3. Writing sorted list of hotels to a JSON file.
4. Searching for hotels in a specific city.
5. Searching for cities that have hotels with a given name.
