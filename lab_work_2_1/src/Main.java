import java.io.*;
import java.util.*;

public class Main {
    public static void readMatrix(int[][] a, int n, Scanner in) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++){
                a[i][j] = in.nextInt();
            }
        }
    }

    public static void main(String[] args) throws IOException {
        FileReader fr = new FileReader("input.txt");
        FileWriter fw = new FileWriter("output.txt");
        Scanner in = new Scanner(fr);

        int n = in.nextInt();
        int[][] a = new int[n][n], b = new int[n][n];

        readMatrix(a, n, in);
        readMatrix(b, n, in);

        int[] products = new int[n];
        for (int i = 0; i < n; i++) {
            products[i] = Arrays.stream(b[i]).reduce(1, (x, y) -> x * y);
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                fw.write(a[i][j] + products[j] + " ");
            }
            fw.write("\n");
        }

        fr.close();
        fw.close();
    }
}