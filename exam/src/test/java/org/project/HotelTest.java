package org.project;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HotelTest {
    private static final Hotel hotel = new Hotel("Minsk", "Zviazda", 4);

    @org.junit.jupiter.api.Test
    void getCityTest() {
        assertEquals("Minsk", hotel.getCity());
    }

    @org.junit.jupiter.api.Test
    void setCityTest() {
        Hotel testHotel = new Hotel("Gomel", "Gogomel", 3);
        testHotel.setCity("Minsk");
        Hotel expectedHotel = new Hotel("Minsk", "Gogomel", 3);
        assertEquals(expectedHotel, testHotel);
    }

    @org.junit.jupiter.api.Test
    void getNameTest() {
        assertEquals("Zviazda", hotel.getName());
    }

    @org.junit.jupiter.api.Test
    void setNameTest() {
        Hotel testHotel = new Hotel("Gomel", "Gogomel", 3);
        testHotel.setName("Zviazda");
        Hotel expectedHotel = new Hotel("Gomel", "Zviazda", 3);
        assertEquals(expectedHotel, testHotel);
    }

    @org.junit.jupiter.api.Test
    void getStarsTest() {
        assertEquals(4, hotel.getStars());
    }

    @org.junit.jupiter.api.Test
    void setStarsTest() {
        Hotel testHotel = new Hotel("Gomel", "Gogomel", 3);
        testHotel.setStars(4);
        Hotel expectedHotel = new Hotel("Gomel", "Gogomel", 4);
        assertEquals(expectedHotel, testHotel);
    }

    @org.junit.jupiter.api.Test
    void equalsTest() {
        Hotel testHotel = new Hotel("Minsk", "Zviazda", 4);
        assertEquals(hotel, testHotel);
    }

    @org.junit.jupiter.api.Test
    void toStringTest() {
        assertEquals("Hotel{city=Minsk, name=Zviazda, stars=4}", hotel.toString());
    }

    @org.junit.jupiter.api.Test
    void fromStringTest() {
        Hotel testHotel = Hotel.fromString("Gomel Gogomel 3");
        Hotel expectedHotel = new Hotel("Gomel", "Gogomel", 3);
        assertEquals(expectedHotel, testHotel);
    }

    @org.junit.jupiter.api.Test
    void toJsonTest() throws JsonProcessingException {
        assertEquals("{\"city\":\"Minsk\",\"name\":\"Zviazda\",\"stars\":4}", hotel.toJson());
    }

    @org.junit.jupiter.api.Test
    void fromJsonTest() throws JsonProcessingException {
        Hotel testHotel = Hotel.fromJson("{\"city\":\"Gomel\",\"name\":\"Gogomel\",\"stars\":3}");
        Hotel expectedHotel = new Hotel("Gomel", "Gogomel", 3);
        assertEquals(expectedHotel, testHotel);
    }

    @org.junit.jupiter.api.Test
    void nameComparatorTest() {
        Hotel testHotel1 = new Hotel("Gomel", "Gogomel", 3);
        Hotel testHotel2 = new Hotel("Minsk", "Gogomel", 4);
        Hotel testHotel3 = new Hotel("Minsk", "Minsk", 2);
        Comparator<Hotel> comparator = new Hotel.NameComparator();
        assertEquals(0, comparator.compare(testHotel1, testHotel2));
        assertTrue(comparator.compare(testHotel1, testHotel3) < 0);
        assertTrue(comparator.compare(testHotel3, testHotel1) > 0);
    }

    @org.junit.jupiter.api.Test
    void cityComparatorTest() {
        Hotel testHotel1 = new Hotel("Gomel", "Gogomel", 3);
        Hotel testHotel2 = new Hotel("Minsk", "Zviazda", 4);
        Hotel testHotel3 = new Hotel("Minsk", "Belarus", 2);
        Comparator<Hotel> comparator = new Hotel.CityComparator();
        assertTrue(comparator.compare(testHotel1, testHotel2) < 0);
        assertTrue(comparator.compare(testHotel2, testHotel1) > 0);
        assertEquals(0, comparator.compare(testHotel2, testHotel3));
    }

    @org.junit.jupiter.api.Test
    void starsComparatorTest() {
        Hotel testHotel1 = new Hotel("Gomel", "Gogomel", 2);
        Hotel testHotel2 = new Hotel("Minsk", "Zviazda", 2);
        Hotel testHotel3 = new Hotel("Minsk", "Belarus", 3);
        Comparator<Hotel> comparator = new Hotel.StarsComparator();
        assertEquals(0, comparator.compare(testHotel1, testHotel2));
        assertTrue(comparator.compare(testHotel1, testHotel3) < 0);
        assertTrue(comparator.compare(testHotel3, testHotel1) > 0);
    }

    @org.junit.jupiter.api.Test
    void cityStarsComparatorTest() {
        Hotel testHotel1 = new Hotel("Gomel", "Gogomel", 2);
        Hotel testHotel2 = new Hotel("Minsk", "Zviazda", 3);
        Hotel testHotel3 = new Hotel("Minsk", "Belarus", 3);
        Hotel testHotel4 = new Hotel("Minsk", "Belarus", 4);
        Comparator<Hotel> comparator = new Hotel.CityStarsComparator();
        assertTrue(comparator.compare(testHotel1, testHotel2) < 0);
        assertTrue(comparator.compare(testHotel2, testHotel1) > 0);
        assertTrue(comparator.compare(testHotel3, testHotel4) > 0);
        assertTrue(comparator.compare(testHotel4, testHotel3) < 0);
        assertEquals(0, comparator.compare(testHotel2, testHotel3));
    }
}