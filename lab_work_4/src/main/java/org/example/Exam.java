package org.example;

import java.util.Comparator;

public class Exam {
    private String name;
    private int grade;
    private String examinator;

    public String getExaminator() {
        return examinator;
    }

    public void setExaminator(String examinator) {
        this.examinator = examinator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public Exam() {
        this.name = "";
        this.grade = 0;
    }

    public Exam(String examinator, String name, int grade) {
        this.examinator = examinator;
        this.name = name;
        this.grade = grade;
    }

    public static Exam parse(String string) {
        String[] strings = string.split("\\. ");
        return new Exam(strings[0], strings[1], Integer.parseInt(strings[2]));
    }

    @Override
    public String toString() {
        return String.format("{examinator: %s, name: %s, grade: %d}", examinator, name, grade);
    }

    public static class GradeComparator implements Comparator<Exam> {
        @Override
        public int compare(Exam o1, Exam o2) {
            return o1.getGrade() - o2.getGrade();
        }
    }

    public static class NameComparator implements Comparator<Exam> {
        @Override
        public int compare(Exam o1, Exam o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }

    public static class ExaminatorComparator implements Comparator<Exam> {
        @Override
        public int compare(Exam o1, Exam o2) {
            return o1.getExaminator().compareTo(o2.getExaminator());
        }
    }
}
