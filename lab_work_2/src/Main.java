import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        FileReader fr = new FileReader("input.txt");
        FileWriter fw = new FileWriter("output.txt");
        Scanner in = new Scanner(fr);

        int n = in.nextInt(), m = in.nextInt();
        int[][] Matrix = new int[n][m];

        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                Matrix[i][j] = in.nextInt();

        int cnt = 0;
        if (m == 10) {
            for (int i = 0; i < n; i++) {
                Arrays.sort(Matrix[i]);
                boolean flag = true;
                for (int j = 0; j < m; j++) {
                    if (Matrix[i][j] != j - 10) {
                        flag = false;
                        break;
                    }
                }
                if (flag) ++cnt;
            }
        }

        fw.write("Количество строк, являющихся перестановками последовательности -10, -9, ..., -2, -1: " + String.valueOf(cnt));

        fr.close();
        fw.close();
    }
}