import java.io.*;


public class Main {
    public static void main(String[] args) {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        try {
            String line;
            while (!(line = br.readLine()).isEmpty()) {
                boolean saw_space = false, prev_is_letter = true;
                StringBuilder result = new StringBuilder();
                int i = 0;
                while (i < line.length() && !Character.isLetter(line.charAt(i)) && !Character.isSpaceChar(line.charAt(i))) {
                    ++i;
                }
                for (; i < line.length(); i++) {
                    char c = line.charAt(i);
                    if (Character.isLetter(c)) {
                        if (!saw_space && !prev_is_letter) {
                            result.append(' ');
                        }
                        result.append(c);
                        saw_space = false;
                        prev_is_letter = true;
                    } else if (Character.isSpaceChar(c)) {
                        result.append(c);
                        saw_space = true;
                        prev_is_letter = false;
                    } else {
                        prev_is_letter = false;
                    }
                }
                System.out.println(result);
            }
        } catch (IOException e) {
            System.err.println("Произошла ошибка во время чтения строки");
        }
    }
}