package com.example.calculator;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.*;
import javafx.event.*;

public class Controller {
    private long num1 = 0;
    private long num2 = 0;
    private Operator operator = Operator.NOP;

    @FXML
    private Text aboveText;

    @FXML
    private Text text;

    @FXML
    private void digitButtonPressed(ActionEvent event) {
        long digit = Long.parseLong(((Button)event.getSource()).getText());
        if (num2 >= 0) {
            num2 = num2 * 10 + digit;
        } else {
            num2 = num2 * 10 - digit;
        }
        text.setText(String.valueOf(num2));
    }

    @FXML
    private void operatorButtonPressed(ActionEvent event) {
        String operatorString = ((Button)event.getSource()).getText();
        if (operator == Operator.NOP) {
            num1 = num2;
            num2 = 0;
            text.setText(String.valueOf(num2));
        }
        switch (operatorString) {
            case "+":
                operator = Operator.ADD;
                break;
            case "-":
                operator = Operator.SUB;
                break;
            case "*":
                operator = Operator.MUL;
                break;
            case "/":
                operator = Operator.DIV;
                break;
            default:
                operator = Operator.NOP;
                break;
        }
        if (operator != Operator.NOP) {
            aboveText.setText(String.valueOf(num1) + " " + operator.toString());
        }
    }

    @FXML
    private void equalsButtonPressed() {
        num2 = Model.calculation(num1, num2, operator);
        num1 = 0;
        operator = Operator.NOP;
        aboveText.setText("");
        text.setText(String.valueOf(num2));
    }

    @FXML
    private void deleteButtonPressed() {
        num2 /= 10;
        text.setText(String.valueOf(num2));
    }
}