package com.example.calculator;

public class Model {
    public static long calculation(long a, long b, Operator operator) {
        return switch (operator) {
            case ADD -> a + b;
            case SUB -> a - b;
            case MUL -> a * b;
            case DIV -> (b == 0 ? 0 : a / b);
            default -> 0;
        };
    }
}

