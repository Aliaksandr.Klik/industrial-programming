package com.example.vaadin_mini;

import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaadinMiniApplication {

    public static void main(String[] args) {
        SpringApplication.run(VaadinMiniApplication.class, args);
    }
}
