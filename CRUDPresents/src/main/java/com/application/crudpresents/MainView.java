package com.application.crudpresents;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import org.vaadin.crudui.crud.impl.GridCrud;

@Route("")
public class MainView extends VerticalLayout {
    public MainView(ManufacturerService service) {
        var table = new GridCrud<>(Manufacturers.class, service);
        table.getGrid().setColumns("manufacturer", "present", "price");
        table.getCrudFormFactory().setVisibleProperties("manufacturer", "present", "price");
        add(table);
    }
}
