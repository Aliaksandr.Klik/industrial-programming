package com.example.vaadin_mini;

import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.*;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("")
class MainView extends VerticalLayout {
    MainView() {
        add(new H1("Hello, world"));
        H2 currentColor = new H2("Current color: None");
        ComboBox<String> comboBox = new ComboBox<>("Select a color");
        comboBox.setItems("Red", "Green", "Blue", "Yellow");
        comboBox.addValueChangeListener(event -> {
            String color = event.getValue();
            if (color == null) {
                currentColor.setText("Current color: None");
            } else {
                currentColor.setText("Current color: " + color);
            }
        });
        add(comboBox);
        add(currentColor);
    }
}