package com.application.crudpresents;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudPresentsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrudPresentsApplication.class, args);
    }

}
