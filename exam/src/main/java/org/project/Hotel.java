package org.project;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Hotel {
    public static final String HotelRegex = "(\\w+) (\\w+) (\\d+)";
    private String city;
    private String name;
    int stars;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public Hotel() {
        city = "";
        name = "";
        stars = -1;
    }

    public Hotel(String city, String name, int stars) {
        this.city = city;
        this.name = name;
        this.stars = stars;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (!(other instanceof Hotel otherHotel)) return false;
        return city.equals(otherHotel.city) && name.equals(otherHotel.name) && stars == otherHotel.stars;
    }

    @Override
    public String toString() {
        return "Hotel{" + "city=" + city + ", name=" + name + ", stars=" + stars + '}';
    }

    public static Hotel fromString(String string) throws IllegalArgumentException {
        Pattern pattern = Pattern.compile(HotelRegex);
        Matcher matcher = pattern.matcher(string);
        if (!matcher.find()) throw new IllegalArgumentException("Invalid Hotel string: " + string);
        String city = matcher.group(1);
        String name = matcher.group(2);
        int stars = Integer.parseInt(matcher.group(3));
        return new Hotel(city, name, stars);
    }

    public String toJson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(this);
    }

    public static Hotel fromJson(String hotelJson) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(hotelJson, Hotel.class);
    }

    public static class NameComparator implements Comparator<Hotel> {
        @Override
        public int compare(Hotel h1, Hotel h2) {
            return h1.getName().compareTo(h2.getName());
        }
    }

    public static class CityComparator implements Comparator<Hotel> {
        @Override
        public int compare(Hotel h1, Hotel h2) {
            return h1.getCity().compareTo(h2.getCity());
        }
    }

    public static class StarsComparator implements Comparator<Hotel> {
        @Override
        public int compare(Hotel h1, Hotel h2) {
            return h1.getStars() - h2.getStars();
        }
    }

    public static class CityStarsComparator implements Comparator<Hotel> {
        @Override
        public int compare(Hotel h1, Hotel h2) {
            if (h1.getCity().equals(h2.getCity())) {
                return h2.getStars() - h1.getStars();
            }
            return h1.getCity().compareTo(h2.getCity());
        }
    }
}
