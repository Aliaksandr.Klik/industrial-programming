# Lab work 2

This is a Java program that performs operations on a matrix.

## Description

The program reads an `n x m` matrix from the input, where `n` is the number of rows and `m` is the number of columns. Each element of the matrix is an integer.

The program then counts the number of rows that are permutations of the sequence -10, -9, ..., -2, -1.
