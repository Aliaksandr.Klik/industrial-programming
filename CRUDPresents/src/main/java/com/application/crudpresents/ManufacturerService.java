package com.application.crudpresents;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.vaadin.crudui.crud.CrudListener;

@Service
@RequiredArgsConstructor
public class ManufacturerService implements CrudListener<Manufacturers> {
    private final ManufacturerRepository repository;

    @Override
    public Manufacturers add(Manufacturers manufacturers) {
        return repository.save(manufacturers);
    }

    @Override
    public java.util.List<Manufacturers> findAll() {
        return repository.findAll();
    }

    @Override
    public Manufacturers update(Manufacturers manufacturers) {
        return repository.save(manufacturers);
    }

    @Override
    public void delete(Manufacturers manufacturers) {
        repository.delete(manufacturers);
    }

}
