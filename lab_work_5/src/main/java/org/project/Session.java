package org.project;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

public class Session {
    int sessionNumber;
    ArrayList<Exam> exams;
    ArrayList<Credit> credits;

    public int getSessionNumber() {
        return sessionNumber;
    }

    public void setSessionNumber(int sessionNumber) {
        this.sessionNumber = sessionNumber;
    }

    public ArrayList<Exam> getExams() {
        return exams;
    }

    public void setExams(ArrayList<Exam> exams) {
        this.exams = exams;
    }

    public ArrayList<Credit> getCredits() {
        return credits;
    }

    public void setCredits(ArrayList<Credit> credits) {
        this.credits = credits;
    }

    public Session() {
        this.sessionNumber = -1;
        this.exams = null;
        this.credits = null;
    }

    public Session(int sessionNumber, ArrayList<Exam> exams, ArrayList<Credit> credits) {
        this.sessionNumber = sessionNumber;
        this.exams = exams;
        this.credits = credits;
    }

    @Override
    public String toString() {
        return "Session{" +
                "\n\tsessionNumber=" + sessionNumber +
                "\n\texams=" + exams +
                ",\n\tcredits=" + credits +
                "\n}";
    }

    public String toJson() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(this);
    }

    public static Session fromJson(String sessionJson) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(sessionJson, Session.class);
    }
}
