package org.project;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        String filename = "hotel.txt";
        String JsonFilename = "hotel.json";
        File file = new File(filename);
        Scanner sc;
        try {
            sc = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("File " + filename + " not found");
            return;
        }

        ArrayList<Hotel> hotels = new ArrayList<>();
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            try {
                hotels.add(Hotel.fromString(line));
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        }

        System.out.println("Initial list of hotels:");
        for (Hotel hotel : hotels) {
            System.out.println(hotel);
        }

        hotels.sort(new Hotel.CityStarsComparator());

        System.out.println("\nSorted list of hotels:");
        for (Hotel hotel : hotels) {
            System.out.println(hotel);
        }

        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File(JsonFilename), hotels);
        } catch (IOException e) {
            System.out.println("Error writing to JSON file: " + e.getMessage());
        }

        int choice = -1;
        Scanner in = new Scanner(System.in);
        do {
            System.out.println("\nChoose an option:");
            System.out.println("0. Exit");
            System.out.println("1. Find hotels in a city");
            System.out.println("2. Find cities that have hotels with given name");
            System.out.print("Your choice: ");
            try {
                choice = in.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Invalid input. Please only enter a number.");
                in.next();
                continue;
            }

            switch (choice) {
                case 0:
                    System.out.println("Exiting...");
                    break;
                case 1:
                    System.out.print("Enter city name: ");
                    String city = in.next();
                    int index = Collections.binarySearch(hotels, new Hotel(city, "", 0), new Hotel.CityComparator());
                    if (index < 0) {
                        System.out.println("No hotels found in " + city);
                    } else {
                        while (index > 0 && hotels.get(index - 1).getCity().equals(city)) --index;
                        System.out.println("Hotels found in " + city + ":");
                        while (index < hotels.size() && hotels.get(index).getCity().equals(city)) {
                            System.out.println(hotels.get(index));
                            ++index;
                        }
                    }
                    break;
                case 2:
                    System.out.print("Enter hotel name: ");
                    String name = in.next();
                    TreeSet<String> cities = new TreeSet<>();
                    for (Hotel hotel : hotels) {
                        if (hotel.getName().equals(name)) {
                            cities.add(hotel.getCity());
                        }
                    }
                    if (cities.isEmpty()) {
                        System.out.println("No hotels found with name " + name);
                    } else {
                        System.out.println("Cities that have hotel with name " + name + ":");
                        for (String c : cities) {
                            System.out.println(c);
                        }
                    }
                    break;
                default:
                    System.out.println("Invalid choice. Please enter a number between 0 and 2.");
            }
        } while (choice != 0);
    }
}