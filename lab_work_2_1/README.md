# Lab work 2_1

This is a Java project that reads two square matrices of order n from an input file, calculates the product of elements in each row of the second matrix, and adds these products to the corresponding columns of the first matrix. The result is written to an output file.
